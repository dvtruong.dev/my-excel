import Vue from 'vue';
import AppButton from '~/components/common/AppButton';
import Loading from '~/components/common/Loading';


Vue.component('app-button', AppButton)
Vue.component('loading', Loading)
